## Documentation

### Description

This project was succesfully integrated with `eslint`, `nyc`, `pre-push`, `winston` and `artillery` \
Postman collections: https://www.getpostman.com/collections/7a9e5e48f02255369622

#### Starting
1. Run `npm install`
2. After that, run `npm start`. The application should be served in `http://localhost:8010`

#### Run linter
Run `npm run lint`

#### Run test
Run `npm test`

#### Run coverage
Run `npm run coverage`

#### Run artillery
Run `npm run test:load` \

For more information, please visit and read the installation instructions in https://www.artillery.io/docs/guides/getting-started/installing-artillery