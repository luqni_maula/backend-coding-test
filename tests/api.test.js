'use strict';

const request = require('supertest');

const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database(':memory:');

const app = require('../src/app')(db);
const buildSchemas = require('../src/schemas');

describe('API tests', () => {
    before((done) => {
        db.serialize((err) => { 
            if (err) {
                return done(err);
            }

            buildSchemas(db);

            done();
        });
    });

    describe('GET /health', () => {
        it('should return health', (done) => {
            request(app)
                .get('/health')
                .expect('Content-Type', /text/)
                .expect(200, done);
        });
    });
    
    describe('POST /rides', () => {
        it('should return success and inserted data', (done) => {
            const payload = {
                start_lat: 10,
                start_long: 10,
                end_lat: 50,
                end_long: 60,
                rider_name: "Luqni Maulana (Rider)",
                driver_name: "Luqni Maulana (Driver)",
                driver_vehicle: "Honda"
            };

            request(app)
                .post('/rides')
                .send(payload)
                .set('Accept', 'application/json')
                .expect((res) => {
                    res.body.data = {
                        startLat: res.body.data.startLat,
                        startLong: res.body.data.startLong,
                        endLat: res.body.data.endLat,
                        endLong: res.body.data.endLong,
                        riderName: res.body.data.riderName,
                        driverName: res.body.data.driverName,
                        driverVehicle: res.body.data.driverVehicle
                    };
                }).expect(201, {
                    success: true,
                    data: {
                        startLat: payload.start_lat,
                        startLong: payload.start_long,
                        endLat: payload.end_lat,
                        endLong: payload.end_long,
                        riderName: payload.rider_name,
                        driverName: payload.driver_name,
                        driverVehicle: payload.driver_vehicle
                    }
                }, done);
        });
        
        it('should error when start latitude and longitude not between -90 - 90 and -180 to 180 degrees', (done) => {
            const payload = {
                start_lat: 190,
                start_long: 200,
                end_lat: 50,
                end_long: 60,
                rider_name: "Luqni Maulana (Rider)",
                driver_name: "Luqni Maulana (Driver)",
                driver_vehicle: "Honda"
            };

            request(app)
                .post('/rides')
                .send(payload)
                .set('Accept', 'application/json')
                .expect(400, done);
        });

        it('should error when end latitude and longitude not between -90 - 90 and -180 to 180 degrees', (done) => {
            const payload = {
                start_lat: 10,
                start_long: 10,
                end_lat: 190,
                end_long: 200,
                rider_name: "Luqni Maulana (Rider)",
                driver_name: "Luqni Maulana (Driver)",
                driver_vehicle: "Honda"
            };

            request(app)
                .post('/rides')
                .send(payload)
                .set('Accept', 'application/json')
                .expect(400, done);
        });
        
        it('should error when rider name empty', (done) => {
            const payload = {
                start_lat: 10,
                start_long: 10,
                end_lat: 50,
                end_long: 60,
                rider_name: "",
                driver_name: "Luqni Maulana (Driver)",
                driver_vehicle: "Honda"
            };

            request(app)
                .post('/rides')
                .send(payload)
                .set('Accept', 'application/json')
                .expect(400, done);
        });

        it('should error when driver name empty', (done) => {
            const payload = {
                start_lat: 10,
                start_long: 10,
                end_lat: 50,
                end_long: 60,
                rider_name: "Luqni Maulana (Rider)",
                driver_name: "",
                driver_vehicle: "Honda"
            };

            request(app)
                .post('/rides')
                .send(payload)
                .set('Accept', 'application/json')
                .expect(400, done);
        });

        it("should error when driver's vehicle name empty", (done) => {
            const payload = {
                start_lat: 10,
                start_long: 10,
                end_lat: 50,
                end_long: 60,
                rider_name: "Luqni Maulana (Rider)",
                driver_name: "Luqni Maulana (Driver)",
                driver_vehicle: ""
            };

            request(app)
                .post('/rides')
                .send(payload)
                .set('Accept', 'application/json')
                .expect(400, done);
        });

        it("should error throw query string errorDebugging = 1", (done) => {
            const payload = {
                start_lat: 10,
                start_long: 10,
                end_lat: 50,
                end_long: 60,
                rider_name: "Luqni Maulana (Rider)",
                driver_name: "Luqni Maulana (Driver)",
                driver_vehicle: "Honda"
            };

            request(app)
                .post('/rides?errorDebugging=1')
                .send(payload)
                .set('Accept', 'application/json')
                .expect(400, done);
        });
    });
    
    describe('GET /rides', () => {
        it('should return 200 OK', (done) => {
            request(app)
                .get('/rides?page=1&limit=10')
                .expect('Content-Type', /json/)
                .expect(200, done);
        });

        it('should error when page query string contains character except number', (done) => {
            request(app)
                .get('/rides?page=1a&limit=10')
                .expect('Content-Type', /json/)
                .expect(400, done);
        });
        
        it('should error when limit query string contains character except number', (done) => {
            request(app)
                .get('/rides?page=1&limit=10a')
                .expect('Content-Type', /json/)
                .expect(400, done);
        });

        it('should OK when page = 2', (done) => {
            request(app)
                .get('/rides?page=2&limit=10')
                .expect('Content-Type', /json/)
                .expect(200, done);
        });

        it('should error when throw query string errorDebugging = 1', (done) => {
            request(app)
                .get('/rides?page=1&limit=10&errorDebugging=1')
                .expect('Content-Type', /json/)
                .expect(400, done);
        });
    });

    describe('GET /rides/:id', () => {
        const id = 1;
        it('should return success and object data', (done) => {
            request(app)
                .get('/rides/' + id)
                .expect('Content-Type', /json/)
                .expect((res) => {
                    res.body.data = {
                        rideID: res.body.data.rideID === id ? id : 0
                    };
                }).expect(200, {
                    success: true,
                    data: {
                        rideID: id
                    }
                }, done);
        });

        it('should error when data not found', (done) => {
            request(app)
                .get('/rides/2')
                .expect('Content-Type', /json/)
                .expect(400, done);
        });

        it('should error when throw query string errorDebugging = 1', (done) => {
            request(app)
                .get('/rides/1?errorDebugging=1')
                .expect('Content-Type', /json/)
                .expect(400, done);
        });
    });
});