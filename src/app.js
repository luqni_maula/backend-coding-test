'use strict';

const express = require('express');
const app = express();

const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

const Rides = require('./rides');
const Logger = require('./util/winston');

module.exports = (db) => {
    app.get('/health', (req, res) => res.send('Healthy'));

    app.post('/rides', jsonParser, async (req, res) => {
        const startLatitude = Number(req.body.start_lat);
        const startLongitude = Number(req.body.start_long);
        const endLatitude = Number(req.body.end_lat);
        const endLongitude = Number(req.body.end_long);
        const riderName = req.body.rider_name;
        const driverName = req.body.driver_name;
        const driverVehicle = req.body.driver_vehicle;
        const errorDebugging = req.query.errorDebugging == 1;

        if (startLatitude < -90 || startLatitude > 90 || startLongitude < -180 || startLongitude > 180)
        {
            const response = {
                success: false,
                code: 'VALIDATION_ERROR',
                message: 'Start latitude and longitude must be between -90 - 90 and -180 to 180 degrees respectively'
            };
            Logger.logError(response);
            return res.status(400).json(response);
        }

        if (endLatitude < -90 || endLatitude > 90 || endLongitude < -180 || endLongitude > 180)
        {
            const response = {
                success: false,
                code: 'VALIDATION_ERROR',
                message: 'End latitude and longitude must be between -90 - 90 and -180 to 180 degrees respectively'
            };
            Logger.logError(response);
            return res.status(400).json(response);
        }

        if (typeof riderName !== 'string' || riderName.length < 1)
        {
            const response = {
                success: false,
                code: 'VALIDATION_ERROR',
                message: 'Rider name must be a non empty string'
            };
            Logger.logError(response);
            return res.status(400).json(response);
        }

        if (typeof driverName !== 'string' || driverName.length < 1)
        {
            const response = {
                success: false,
                code: 'VALIDATION_ERROR',
                message: 'Rider name must be a non empty string'
            };
            Logger.logError(response);
            return res.status(400).json(response);
        }

        if (typeof driverVehicle !== 'string' || driverVehicle.length < 1)
        {
            const response = {
                success: false,
                code: 'VALIDATION_ERROR',
                message: 'Rider name must be a non empty string'
            };
            Logger.logError(response);
            return res.status(400).json(response);
        }

        try {
            const result = await Rides.create(db, {
                'startLat': startLatitude,
                'startLong': startLongitude,
                'endLat': endLatitude,
                'endLong': endLongitude,
                'riderName': riderName,
                'driverName': driverName,
                'driverVehicle': driverVehicle
            }, errorDebugging);

            return res.status(201).json({
                success: true,
                data: result
            });
        } catch (err) {
            Logger.logError(err);
            return res.status(400).json({
                success: false,
                ...err
            });
        }
    });

    app.get('/rides', async (req, res) => {
        let page = Number(req.query.page);
        let limit = Number(req.query.limit);
        const errorDebugging = req.query.errorDebugging == 1;

        if (isNaN(page))
        {
            // validation for type data should be valid integer
            const response = {
                success: false,
                code: 'RIDES_LIMIT_QUERY_ERROR',
                message: 'The page query must be an integer'
            };
            Logger.logError(response);
            return res.status(400).json(response);
        }

        if (page < 1)
        {
            // validation for type data should be greater than 1
            const response = {
                success: false,
                code: 'RIDES_PAGE_QUERY_ERROR',
                message: 'The page query must be greater than 1'
            };
            Logger.logError(response);
            return res.status(400).json(response);
        }

        if (isNaN(limit))
        {
            // validation for type data should be valid integer
            const response = {
                success: false,
                code: 'RIDES_LIMIT_QUERY_ERROR',
                message: 'The limit query must be an integer'
            };
            Logger.logError(response);
            return res.status(400).json();
        }

        if (limit < 1)
        {
            // validation for type data should be greater than 1
            const response = {
                success: false,
                code: 'RIDES_LIMIT_QUERY_ERROR',
                message: 'The limit query must be greater than 1'
            };
            Logger.logError(response);
            return res.status(400).json(response);
        }

        try {
            const data = await Rides.get(db, page, limit, errorDebugging);
            const total = await Rides.getTotal(db);

            const numPages = Math.ceil(total / limit);

            return res.status(200).json({
                success: true,
                data,
                total,
                currentPage: page,
                prevPage: page > 0 ? page - 1 : undefined,
                nextPage: page < numPages ? page + 1 : undefined,
            });
        } catch (err) {
            Logger.logError(err);
            return res.status(400).json({
                success: false,
                ...err
            });
        }
    });

    app.get('/rides/:id', async (req, res) => {
        const errorDebugging = req.query.errorDebugging == 1;
        try {
            const row = await Rides.getByID(db, req.params.id, errorDebugging);
            return res.status(200).json({
                success: true,
                data: row
            })
        } catch (err) {
            Logger.logError(err);
            return res.status(400).json({
                success: false,
                ...err
            });
        }
    });

    return app;
};
