'use strict';

const table = 'Rides';
const primaryKey ='rideID';
const fillableColumns = [
	'startLat',
	'startLong',
	'endLat',
	'endLong',
	'riderName',
	'driverName',
	'driverVehicle'
];

const fakeTable = 'Rides_';

module.exports = {
	get: (db, page, limit, errorDebugging = false) =>
	{
		return new Promise((resolve, reject) =>
		{
			const skip = page > 1 ? ((page - 1) * limit) : 0;

			db.all(`SELECT * FROM ${errorDebugging ? fakeTable : table} LIMIT ${skip}, ${limit}`, function (err, rows) {
				if (err)
				{
					return reject({
						code: 'SERVER_ERROR',
						message: 'Unknown error'
					});
				}

				resolve(rows);
			});
		});
	},
	getTotal: (db) =>
	{
		return new Promise((resolve, reject) =>
		{
	        db.all(`SELECT COUNT(*) AS total FROM ${table}`, function (err, rows) {
	            if (err)
	            {
	                return reject({
	                    code: 'SERVER_ERROR',
	                    message: 'Unknown error'
	                });
	            }

	            resolve(parseInt(rows[0].total));
	        });
		});
	},
	getByID: (db, id, errorDebugging = false) =>
	{
		return new Promise((resolve, reject) =>
		{
			db.all(`SELECT * FROM ${errorDebugging ? fakeTable : table} WHERE ${primaryKey}=?`, id,
			function (err, rows) {
	            if (err)
	            {
	                return reject({
	                    code: 'SERVER_ERROR',
	                    message: 'Unknown error'
	                });
	            }

	            if (rows.length > 0)
	            {
	            	resolve(rows[0]);
	            } else {
	                reject({
	                    code: 'RIDES_NOT_FOUND_ERROR',
	                    message: 'Could not find any rides'
	                });
	            }
	        });
		});
	},
	create: (db, payload, errorDebugging =  false) =>
	{
		return new Promise((resolve, reject) =>
		{
			// if (!(typeof payload === 'object' && payload !== {}))
			// {
			// 	reject({
			// 		code: 'INVALID_PAYLOAD',
			// 		message: 'The payload arguments should be object and cannot be empty!'
			// 	});
			// }

			// for (let key in payload)
			// {
			// 	if (!fillableColumns.includes(key))
			// 	{
			// 		reject({
			// 			code: 'UNDEFINED_COLUMN',
			// 			message: `The column ${key} is not defined`
			// 		});
			// 	}
			// }

			const values = [];
			fillableColumns.map((column) => values.push(payload[column]));

			db.run(`INSERT INTO ${errorDebugging ? fakeTable : table}(${fillableColumns.join()}) VALUES (${(fillableColumns.map(() => '?')).join()})`, values, function (err)
			{
	            if (err)
	            {
	                return reject({
	                    code: 'SERVER_ERROR',
	                    message: 'Unknown error'
	                });
	            }

	            db.all(`SELECT * FROM ${table} WHERE rideID = ?`, this.lastID, function (err, rows)
	            {
	                resolve(rows[0]);
	            });
	        });
		})
	}
}